import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Main {
	public static void main(String[] args) throws IOException {
		DataInputStream d = new DataInputStream(System.in);
		int n = 0;
		String s;
		while ((s = d.readLine()) != null) {
			n = Integer.parseInt(s);
			count7(n);
		}
	}

	static void count7(int n) {
		int x;
		int count = 0;
		String item = "7";
		String y, str = "";
		StringBuffer strBuff;
		// for(x = 1; x <= n; x++) {
		// 	y = String.valueOf(x) + "0";
		// 	str += y;
		// }
		// int count = str.split("7").length - 1;

		for(x = 1; x <= n; x++) {
			y = String.valueOf(x) + "0";
			str += y;
		}
			strBuff = new StringBuffer(str);
			
		int nowLength = 0;
		while(nowLength < strBuff.length()) {
			int index = strBuff.indexOf(item, nowLength);
			if(index == -1) {
				break;
			}
			nowLength = (index + item.length());
			count ++;
		}
		System.out.println(count);
	}
}
